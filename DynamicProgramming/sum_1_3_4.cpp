#include <iostream>

using namespace std;
int sum_1_3_4_R(int N);
int sum_1_3_4_I(int N);

int main(int argc, char** argv)
{
    int N;
    cin >> N;
    cout << sum_1_3_4_R(N) << endl;
    cout << sum_1_3_4_I(N) << endl;
    return 0;
}

// recursive version
int sum_1_3_4_R(int N)
{
    if(N < 0) return 0;
    if(N < 3) return 1;
    return sum_1_3_4_R(N-1) + sum_1_3_4_R(N-3) + sum_1_3_4_R(N-4);
}

int sum_1_3_4_I(int N)
{
    if(N < 0) return 0;     // 
    if(N < 3) return 1;     // D0, D1, D2
    if(N == 3) return 2;    // D3
    int D0, D1, D2, D3, D4;
    D2 = D1 = D0 = 1;
    D3 = 2;
    for(int i=4; i<=N; i++)
    {
        D4 = D3 + D1 + D0;
        D0 = D1;
        D1 = D2;
        D2 = D3;
        D3 = D4;
    }
    return D4;
}