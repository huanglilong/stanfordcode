/*
longest common subsequence
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;
void lcs(string& a, string& b);

int main(int argc, char** argv)
{
    string a = "ABCBDABBBBC";
    string b = "BDCABC";
    lcs(a,b);
    return 0;
}

void lcs(string& a, string& b)
{
    vector<vector<int>> LCSMatrix(a.size()+1, vector<int>(b.size()+1, 0));
    for(int i=0; i<a.size(); i++)
    {
        for(int j=0; j<b.size(); j++)
        {
            if(a[i] == b[j])
            {
                LCSMatrix[i+1][j+1] = LCSMatrix[i][j] + 1;
            }
            else
            {
                LCSMatrix[i+1][j+1] = max(LCSMatrix[i+1][j], LCSMatrix[i][j+1]);
            }
        }
    }
    cout << LCSMatrix[a.size()][b.size()] << endl;
}