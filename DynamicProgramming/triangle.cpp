/*
http://poj.org/problem?id=1163
*/

#include <iostream>
#include <vector>

using namespace std;
int triangle(int N);

int main(int argc, char** argv)
{
    int N;
    cin >> N;
    cout << triangle(N);
    return 0;
}

int triangle(int N)
{
    // 1. input data
    vector<vector<int>> DataMatrix(N, vector<int>());
    int data;
    for(int j=0; j<N; j++)      // row
    {
        for(int i=0; i<=j; i++) // column
        {
            cin >> data;
            DataMatrix[j].push_back(data);
        }
    }
    // 2. init base case, D[0][0] = DataMatrix[0][0]
    for(int j=1; j<N; j++)      // start from line two
    {
        for(int k=0; k<=j; k++)
        {
            if(k==0)
            {
                DataMatrix[j][k] = DataMatrix[j-1][k] + DataMatrix[j][k];
            }
            else if(k==j)
            {
                DataMatrix[j][k] = DataMatrix[j-1][k-1] + DataMatrix[j][k];
            }
            else
            {
                DataMatrix[j][k] = max(DataMatrix[j-1][k-1], DataMatrix[j-1][k]) + DataMatrix[j][k];
            }
        }
    }
    // find max heightest sum
    int maxSum = DataMatrix[N-1][0];
    for(int i=0; i<N; i++)
    {
        if(DataMatrix[N-1][i] > maxSum)
        {
            maxSum = DataMatrix[N-1][i];
        }
        //cout << DataMatrix[N-1][i] << " " << endl;
    }
    return maxSum;
}
