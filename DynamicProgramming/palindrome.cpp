#include <iostream>
#include <string>
#include <vector>

using namespace std;
int palindrome_R(string& a, int lo, int hi);
//int palindrome_I(string& a);

int main(int argc, char** argv)
{
    string a = "Ab3bcd";
    cout << palindrome_R(a, 0, a.size()) << endl;
    //cout << palindrome_I(a) << endl;
    return 0;
}

int palindrome_R(string& a, int lo, int hi)
{
    // recursive base
    if(hi - lo < 2) return 0;

    if(a[lo] == a[hi -1]) // xi = xj
    {
        palindrome_R(a, lo+1, hi-1);
    }
    else
    {
        return min(palindrome_R(a,lo+1, hi), palindrome_R(a, lo, hi-1)) + 1;
    }
}

// int palindrome_I(string& a)
// {
//     vector<vector<int>> DataMatrix(a.size()+1, vector<int>(a.size()+1, 0));
//     for(int t=2; t<a.size(); t++)
//     {
//         for(int i=1, j=t; j<a.size(); i++, j++)
//         {
//             if(a[i] == a[j])
//             {
//                 DataMatrix[i][j] = DataMatrix[i-1][j-1];
//             }
//             if(a[i] != a[j])
//             {
//                 DataMatrix[i][j] = min(DataMatrix[i-1][j], DataMatrix[i][j-1]) + 1;
//             }
//         }
//     }
//     return DataMatrix[a.size()-1][a.size()-1];
//     //cout << DataMatrix[a.size()-1][a.size()-1] << endl;
// }