/*
http://poj.org/problem?id=2136
*/

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>

using namespace std;

void verticalHistgram(string& strBuf, vector<int>& counts);
void formatPrint(vector<int>& counts);
int maxCount(vector<int>& counts);

int main(int argc, char** argv)
{
    vector<int> counts(26, 0);
    string strBuf;
    ifstream file("../characters.txt");
    if(!file)
    {
        cout << "File opening failed\n";
        return EXIT_FAILURE;
    }
    
    while(!file.eof())
    {
        file >> strBuf;
        verticalHistgram(strBuf, counts);
    }
    formatPrint(counts);
    return 0;
}

bool detectCharacter(char s)
{
    if((s >= 'A') && (s <= 'Z'))
    {
        return true;
    }
    return false;
}

void formatPrint(vector<int>& counts)
{
    int max = maxCount(counts);
    for(int k=0; k<max; k++)                   // for every row
    {
        for(int i=0; i<counts.size(); i++)      // for every character
        {
            if(counts[i] >= max-k)
            {
                cout << "*";
            }
            else
            {
                cout << " ";
            }
            if(i != counts.size()-1)
            {
                cout << " ";
            }
        }
        cout << endl;
    }
    for(char i='A'; i<='Z'; i++)
    {
        cout << i;
        if(i !='Z') cout << " ";
    }
    cout << endl;
}

int maxCount(vector<int>& counts)
{
    int max = 0;
    for(auto data : counts)
    {
        if(data > max)
        {
            max = data;
        }
    }
    return max;
}
void verticalHistgram(string& strBuf, vector<int>& counts)
{
    for(int i=0; i<strBuf.size(); i++)
    {
        if(detectCharacter(strBuf[i]))
        {
            counts[int(strBuf[i]-'A')]++;
            //cout << strBuf[i] << " " << counts[int(strBuf[i]-'A')] << endl;
        }
    }
    //formatPrint(counts);
}