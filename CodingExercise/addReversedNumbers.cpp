/*
http://poj.org/problem?id=1504
*/

#include <iostream>
#include <cmath>
#include <vector>

using namespace std;
unsigned int reverseSum(unsigned int a, unsigned int b);

int main(int argc, char** argv)
{
    int N;
    unsigned int a, b;
    vector<unsigned int> results;
    cin >> N;
    while(N-- > 0)
    {
        cin >> a; cin >> b;
        results.push_back(reverseSum(a, b));
    }
    for(int i=0; i<results.size(); i++)
    {
        cout << results[i] << endl;
    }
    return 0;
}

unsigned int reverseNum(unsigned int a)
{
    // calcualte numbers's length
    int a_len = log10(a) + 1;
    
    unsigned int a_reverse = 0;
    unsigned int base = 1;
    unsigned int re_base = pow(10, a_len-1);
    while(a_len-- > 0)
    {
        unsigned int temp = (a / re_base);
        a_reverse += base * temp;
        a -= re_base * temp;
        re_base /= 10;
        base *= 10;
    }
    return a_reverse;
}

unsigned int reverseSum(unsigned int a, unsigned int b)
{
    // calcualte numbers's length
    unsigned int sum = reverseNum(a) + reverseNum(b);
    return reverseNum(sum);
}
