/*
http://poj.org/problem?id=1922
*/

#include <iostream>
#include <vector>
#include <limits>

using namespace std;

struct stRider
{
    double speed_mDs;           // speed in m/s
    int   time_s;               // time in second(s)
    stRider(int t=0, float v=0)
    {
        speed_mDs = v/3.6;     // convert speed from km/h to m/s
        time_s    = t;
    }
};

int main(int argc, char** argv)
{
    // input data
    int N = 2;
    vector<stRider> Riders = { {0, 21}, {34, 22} };
    // int N = 4;
    // vector<stRider> Riders = { {0, 20}, {-155, 25}, {190, 27}, {240, 30} };
    stRider currRider;
    // total time and current distance
    int total_time = 0;     // in seconds
    int curr_dist = 0;      // in m

    // 1. find init start time and start speed
    int start_pos = numeric_limits<int>::max();
    for(int i=0; i<N; i++)   // find the smallest start time Ti
    {
        if((Riders[i].time_s < start_pos) && (Riders[i].time_s >= 0))
        {
            start_pos = i;
        }
    }
    currRider.time_s        = Riders[start_pos].time_s;
    currRider.speed_mDs     = Riders[start_pos].speed_mDs;
    total_time              += currRider.time_s;     // wait time
    curr_dist               = 0;
    cout << "first Rider: " << "(" << currRider.time_s << ", " << currRider.speed_mDs << ")" << endl;

    // 2. calculate the nearest cross point
    while(curr_dist < 4500)
    {
        int next_cross = 0;
        double nearest_cross = numeric_limits<float>::max();
        for(int i=0; i<N; i++)
        {
            double cross_time = 0;
            if(Riders[i].speed_mDs > currRider.speed_mDs)
            {
                cross_time = (Riders[i].speed_mDs * Riders[i].time_s - currRider.speed_mDs * currRider.time_s) /
                             (Riders[i].speed_mDs - currRider.speed_mDs);
                if(cross_time < nearest_cross)
                {
                    nearest_cross = cross_time;
                    next_cross = i;
                }
            }
        }
        double delta_time = nearest_cross - currRider.time_s;
        double delta_dist = delta_time * currRider.speed_mDs;
        if(curr_dist + delta_dist > 4500)                       // arrive before cross with faster one
        {
            delta_time = (4500 - curr_dist) / currRider.speed_mDs;
            curr_dist = 4500;                                   // finished
        }
        else
        {
            curr_dist += delta_time * currRider.speed_mDs;          // update distance info
            currRider.speed_mDs = Riders[next_cross].speed_mDs;     // follow a faster rider
            currRider.time_s = nearest_cross;                       // keep the cross point time 
        }
        total_time += delta_time;
        cout << "faster Rider: " << "(" << currRider.time_s << ", " << currRider.speed_mDs << ")" << endl;       
    }
    // finish calculate
    cout << "arrive time: " << total_time << endl;

    return 0;
}
