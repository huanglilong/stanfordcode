#include <iostream>
#include <string>

using namespace std;

string strMux(string& stra, string& strb);
string strAdd(string& stra, string& strb);

int main(int argc, char** argv)
{
    string a = "4745";
    string b = "64569";
    cout << 4745 * 64569 << endl;
    cout << strMux(a, b) << endl;
    cout << 4745 + 64569 << endl;
    cout << strAdd(a, b) << endl;
    return 0;
}

string strMux(string& stra, string& strb)
{
    int alen = stra.size();
    int blen = strb.size();
    int counts = 0;
    string result = "0";
    while(blen-- > 0)
    {
        string strMux;                      // save each time multiply result
        for(int i=0; i<counts; i++)
        {
            strMux.push_back('0');          // multipy 10
        }
        counts++;

        int b = int(strb[blen] - '0');      // convert char into int
        cout << "b is " << b << endl;
        int i = alen;
        int carry = 0;
        // multipy each numbers, 321 * 21 = 321 * (20 + 1)
        while(i-- > 0)
        {
            int a = int(stra[i] - '0');
            int mux = a * b + carry;
            carry = mux / 10;
            strMux.insert(strMux.begin(), 1, char(mux % 10 + '0'));  // convert int into char, and save to strMux
        }
        // add carry
        if(carry){ strMux.insert(strMux.begin(), 1, char(carry + '0')); }
        cout << "strMux " << strMux << endl;

        // add strMux to result
        result = strAdd(result, strMux);
    }
    return result;
}

string strAdd(string& stra, string& strb)
{
    int alen = stra.size();
    int blen = strb.size();
    int carry = 0;
    string result;
    while((alen > 0) && (blen > 0))
    {
        alen--; blen--;
        int a = int(stra[alen] - '0');
        int b = int(strb[blen] - '0');
        int sum = a + b + carry;
        carry = sum / 10;
        result.insert(result.begin(), 1, char(sum % 10) + '0');
    }
    while(alen-- > 0)
    {
        int a = int(stra[alen] - '0');
        int sum = a + carry;
        carry = sum / 10;
        result.insert(result.begin(), 1, char(sum % 10) + '0');
    }
    while(blen-- > 0)
    {
        int b = int(strb[blen] - '0');
        int sum = b + carry;
        carry = sum / 10;
        result.insert(result.begin(), 1, char(sum % 10) + '0');
    }
    if(carry){ result.insert(result.begin(), 1, char(carry + '0')); }

    return result;
}