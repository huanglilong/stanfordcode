/*
http://poj.org/problem?id=2140
Write a program that will compute the number of ways farmer John can sum the numbers on consecutive cows to equal N.
Do not use precomputation to solve this problem. 
*/

#include <iostream>
#include <cmath>

using namespace std;

int herdSum(unsigned long int N);

int main(int argc, char** argv)
{
    unsigned long int N;
    cin >> N;
    cout << herdSum(N);
    return 0;
}

int herdSum(unsigned long int N)
{
    unsigned long int sqrt_N = sqrt(2*N) + 1;
    unsigned long int base;
    int counts = 0;
    for(unsigned long int i=1; i<sqrt_N; i++)
    {
        if((2*N % i) == 0)    // no remainder
        {
            base = (N - i*(i-1)/2) / i;
            unsigned long int sum = i*base + i*(i-1)/2;
            if(N == sum)
            {
                counts++;
            }
        }
    }
    return counts;
}