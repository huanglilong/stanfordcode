/*
http://poj.org/problem?id=1007
*/

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <tuple>

using namespace std;
void DNASort(int n, int m);
int invPairs(string strBuf);

int main(int argc, char** argv)
{
    DNASort(6, 1);
    return 0;
}


/**
 * input:
 *      n       : the length of string
 *      m       : the number of string
 *      strings : input m*n strings
 * output:
 *      sorted
 */
/**
 * steps:
 * 1. data for output, vector<vector<string>>
 * 2. calcualte inversion pair numbers
 * 3. insert string into data
 * 4. print all strings
 */
void DNASort(int n, int m)
{
    // 1. defination
    multimap<int, string> data_map;
    string strBuf;
    int pairs = 0;
    for(int i=0; i<n; i++)
    {
        cin >> strBuf;
        pairs = invPairs(strBuf);
        pair<int, string> temp(pairs, strBuf);
        data_map.insert(temp);
    }
    for(auto data : data_map)
    {
        cout << data.first << " " << data.second << endl;
    }
    // 2. calculate inversion pairs's numbers

}

int invPairs(string strBuf)
{
    int counts = 0;
    for(int j=0; j < strBuf.size(); j++)
    {
        for(int i=j+1; i < strBuf.size(); i++)
        {
            if(strBuf[j] > strBuf[i])
            {
                counts++;
            }
        }
    }
    return counts;
}