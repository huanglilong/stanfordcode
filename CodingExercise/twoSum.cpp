#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int towSum(int a, int b);

int main(int argc, char** argv)
{
    srand(time(nullptr));
    int a = rand() % 1000;
    int b = rand() % 1000;
    cout << "a, b " << a << ", " << b << endl; 
    cout << "a + b = " << towSum(a, b) << endl;
    return 0;
}

int towSum(int a, int b)
{
    return a+b;
}