/*
http://poj.org/problem?id=1003
How far can you make a stack of cards overhang a table?
If you have one card, you can create a maximum overhang of half a card length.
(We're assuming that the cards must be perpendicular to the table.)
With two cards you can make the top card overhang the bottom one by half a card length, 
and the bottom one overhang the table by a third of a card length, 
for a total maximum overhang of 1/2 + 1/3 = 5/6 card lengths. 
In general you can make n cards overhang by 1/2 + 1/3 + 1/4 + ... + 1/(n + 1) card lengths, 
where the top card overhangs the second by 1/2, the second overhangs tha third by 1/3, 
the third overhangs the fourth by 1/4, etc., and the bottom card overhangs the table by 1/(n + 1). 
This is illustrated in the figure below.
*/
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <limits>

using namespace std;

int hangOver(float);

int main(int argc, char** argv)
{
    float buf;
    cin >> buf;
    while(buf > numeric_limits<float>::min())
    {
        cout << hangOver(buf) << " " << "card(s)" << endl;
        cin >> buf;
    }
    return 0;
}

int hangOver(float length)
{
    int n = 0;
    while(length > numeric_limits<float>::min())
    {
        n++;
        length -= 1.0f / (1.0f + n);
    }
    return n;
}