/*
http://poj.org/problem?id=2262
*/

/*
1. How to generate prime numbers
2. Or how to check a numbers whether prime or not
*/
#include <iostream>
#include <cmath>
#include <vector>

using namespace std;
bool checkPrime(unsigned int number);

int main(int argc, char** argv)
{
    unsigned int N;
    vector<unsigned int> datas;
    cin >> N;
    while(N != 0)
    {
        datas.push_back(N);
        cin >> N;
    }
    for(int k=0; k<datas.size(); k++)
    {
        if(datas[k] % 2 != 0) return 0;
        bool goldbach_flag = false;
        for(int i=datas[k]-1; i >1; i=i-2)
        {
            if(checkPrime(i) && checkPrime(datas[k]-i))
            {
                cout << datas[k] << " = " << datas[k]-i << " + " << i << endl;
                goldbach_flag = true;
                break;
            }
        }
        if(!goldbach_flag)
        {
            cout << "Goldbach's conjecture is wrong." << endl;
        }    
    }
    return 0;
}

bool checkPrime(unsigned int number)
{
    if(number <=1)  return false;
    for(int i=2; i<sqrt(number); i++)
    {
        if(number % i == 0)
        {
            return false;
        }
    }
    return true;
}