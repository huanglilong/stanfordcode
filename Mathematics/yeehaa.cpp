/*
http://poj.org/problem?id=1799
*/

#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip>

using namespace std;

/**
 * intput:
 *          arg1: N the number of pairs input
 *          arg2: R is the circle's radius, is real number(double)
 *          arg3: n is the number of small circles
 * output:  r, the small circle's radius
 */
const double pi = acos(-1);
int main(int argc, char** argv)
{
    int N;
    cin >> N;
    vector<double> data;
    for(int i=0; i<N; i++)
    {
        double R;
        int n;
        cin >> R;
        cin >> n;
        double temp = sin(pi/n);
        double r = R * temp / (1.0 + temp);
        data.push_back(r);
    }
    for(int i=0; i<data.size(); i++)
    {
        cout << "Scenario #" << i+1 << ":" << endl;
        cout.setf(ios::fixed);
        cout << setprecision(3) << setw(4) << data[i] << endl;
    }
    return 0;
}
