/*
http://poj.org/problem?id=1401
*/

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int factorialZeros(unsigned int N);

int main(int argc, char** argv)
{
    int N,times;
    vector<unsigned int> datas;
    cin >> times;
    for(int i=0; i<times; i++)
    {
        cin >> N;
        datas.push_back(N);
    }
    for(int i=0; i<times; i++)
    {
        cout << factorialZeros(datas[i]) << endl;
    }
    return 0;
}

int factorialZeros(unsigned int N)
{
    int i = 1;
    int zeroCounts = 0;
    unsigned int num_5 = 5;
    for(unsigned int i=1; i*5 <= N; i++)
    {
        unsigned int mux_5 = i * 5;
        while(mux_5 % 5 == 0)
        {
            mux_5 /= 5;
            zeroCounts++;
        }
    }
    return zeroCounts;
}